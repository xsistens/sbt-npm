// this bit is important
sbtPlugin := true

organization := "github.xsistens.scalajs.sbt"

name := "sbt-npm"

version := "1.0.0-SNAPSHOT"

scalaVersion := "2.10.6"

scalacOptions ++= Seq("-deprecation", "-feature")

resolvers += Resolver.sonatypeRepo("snapshots")

libraryDependencies ++= Seq(
  "com.typesafe.play" %% "play-json" % "2.4.6"
)

publishMavenStyle := false

addSbtPlugin("org.scala-js" % "sbt-scalajs" % "0.6.9")

addSbtPlugin("com.typesafe.sbt" % "sbt-web" % "1.4.0")

addSbtPlugin("com.typesafe.sbt" % "sbt-js-engine" % "1.1.3")