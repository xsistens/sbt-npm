package github.xsistens.scalajs.sbt.npm

import java.io.{BufferedWriter, FileWriter}

import sbt.{Def, _}
import Keys._

import scala.concurrent.duration._
import com.typesafe.sbt.jse.SbtJsTask
import com.typesafe.sbt.jse.SbtJsEngine
import com.typesafe.sbt.web.SbtWeb
import org.scalajs.sbtplugin.ScalaJSPlugin.AutoImport._
import play.api.libs.json._

case class NpmDependency(version: String, globalName: String, requireName: String)

object SbtNpm extends AutoPlugin {

  import com.typesafe.sbt.web.Import._
  import com.typesafe.sbt.jse.SbtJsTask._
  import com.typesafe.sbt.jse.SbtJsEngine.autoImport.JsEngineKeys._

  override def requires = SbtJsEngine && SbtJsTask && SbtWeb

  /**
    * Defines all settings/tasks that get automatically imported,
    * when the plugin is enabled
    */
  object autoImport {
    val bundlePath = settingKey[File]("The map of NPM dependencies.")
    val packageFile = settingKey[File]("")
    val npmDependencies = settingKey[Map[String, NpmDependency]]("The map of NPM dependencies.")
    val bundle = inputKey[Unit]("Bundles NPM Modules")
  }

  import autoImport._

  val packageFileTemplate = scala.io.Source.fromInputStream(getClass.getClassLoader.getResourceAsStream("package.json")).mkString
  val libFileTemplate = scala.io.Source.fromInputStream(getClass.getClassLoader.getResourceAsStream("lib.js")).mkString
  val startFileTemplate = scala.io.Source.fromInputStream(getClass.getClassLoader.getResourceAsStream("start.js")).mkString
  val browserifyFileTemplate = scala.io.Source.fromInputStream(getClass.getClassLoader.getResourceAsStream("browserify.js")).mkString

  val createBundleFilesTask = taskKey[Unit]("creating the bundle files")
  val bundleTask = taskKey[Unit]("bundle")


  /**
    * Provide default settings
    */
  override lazy val projectSettings = Seq(
    jsDependencies += ProvidedJS / "bundle.js",
    bundlePath := baseDirectory.value / "bundle",
    packageFile := baseDirectory.value / "package.json",
    npmDependencies := Map.empty,
    createBundleFilesTask := {
      if(npmDependencies.value.nonEmpty || packageFile.value.exists()) {

        bundlePath.value.mkdirs()
        val libFile = bundlePath.value / "/lib.js"
        val packageProvided = packageFile.value.exists()

        if (!packageProvided) {
          val packageFileWriter = new BufferedWriter(new FileWriter(packageFile.value))
          packageFileWriter.write(packageFileTemplate)
          packageFileWriter.close()
        }

        val providedPackageFile = Json.parse(scala.io.Source.fromFile(packageFile.value).mkString).as[JsObject]

        val (packageMap, libRequireList, libList) = npmDependencies.value.foldLeft(Map("browserify" -> "13.0.0"), Seq.empty[String], Seq.empty[String]) {
          case ((pm, lrl, ll), (libName, npm)) =>
            val libRequireLine = s"""global.${npm.globalName} = require("${npm.requireName}");"""
            val libLine = s""""${npm.globalName}": ${npm.globalName}"""
            (pm + (libName -> npm.version), lrl :+ libRequireLine, ll :+ libLine)
        }
        val createdPackageFile = Json.obj("dependencies" -> Json.toJson(packageMap))
        val merged = createdPackageFile.deepMerge(providedPackageFile)

        val packageFileWriter = new BufferedWriter(new FileWriter(packageFile.value))
        packageFileWriter.write(Json.prettyPrint(merged))
        packageFileWriter.close()

        val libFileWriter = new BufferedWriter(new FileWriter(libFile))
        libFileWriter.write(libFileTemplate.replace("$libRequireList$", libRequireList.mkString).replace("$libList$", libList.mkString(", ")))
        libFileWriter.close()
      }
    },
      bundleTask := {
        if (npmDependencies.value.nonEmpty || packageFile.value.exists()) {

          val startFile = bundlePath.value / "start.js"
          val browserifyFile = bundlePath.value / "/browserify.js"
          val resFile = baseDirectory.value / "src" / "main" / "resources"

          val bundleFile = resFile / "bundle.js"

          val startFileWriter = new BufferedWriter(new FileWriter(startFile))
          startFileWriter.write(startFileTemplate)
          startFileWriter.close()

          val browserifyFileWriter = new BufferedWriter(new FileWriter(browserifyFile))
          browserifyFileWriter.write(browserifyFileTemplate)
          browserifyFileWriter.close()

          val inf = startFile.getAbsolutePath
          val outf = bundleFile.getAbsolutePath
          val modules = (baseDirectory.value / "node_modules").getAbsolutePath

          println(s"Bundling: $inf\n -> $outf")
          executeJs(state.value,
            engineType.value,
            None,
            Seq(modules),
            browserifyFile,
            Seq(inf, outf),
            30.seconds)
          ()
      }
    },
    bundle := Def.sequential(createBundleFilesTask, (npmNodeModules in Assets), bundleTask).value
  )

}
